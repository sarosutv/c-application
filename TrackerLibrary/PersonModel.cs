﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackerLibrary
{
    public class PersonModel
    {
        /// <summary>
        /// First name of the person.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the person.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Emailadress of the person.
        /// </summary>
        public string EmailAdress { get; set; }

        /// <summary>
        /// CellPhone number of the person.
        /// </summary>
        public string CellPhoneNumber { get; set; }
    }
}
