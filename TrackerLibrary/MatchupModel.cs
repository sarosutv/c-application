﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackerLibrary
{
    public class MatchupModel
    {
        /// <summary>
        /// Represents how many enteries there are in 
        /// one tournament.
        /// </summary>
        public List<MatchupEntryModel> Enteries { get; set; } = new List<MatchupEntryModel>();

        /// <summary>
        /// Shows the winner of the match.
        /// </summary>
        public TeamModel Winner { get; set; }
        
        /// <summary>
        /// Shows the matchup round.
        /// </summary>
        public int MatchupRounds { get; set; }

    }
}
