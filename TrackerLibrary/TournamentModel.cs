﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackerLibrary
{
    public class TournamentModel
    {
        /// <summary>
        /// name of the tournament.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        /// The entryfee cost for joining the tournament.
        /// </summary>
        public decimal EntryFee { get; set; }

        /// <summary>
        /// List of number of teams in the tournament.
        /// </summary>
        public List<TeamModel> EnteredTeams { get; set; } = new List<TeamModel>();

        /// <summary>
        /// List of different prizes.
        /// </summary>
        public List<PrizeModel> Prizes { get; set; } = new List<PrizeModel>();

        /// <summary>
        /// List the number of rounds in the tournament.
        /// </summary>
        public List<MatchupModel> Rounds { get; set; } = new List<MatchupModel>();
    }
}
